#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
static bool flag = false;
static int signal_pipe[2];

void* thread_sleeper(void* param)
{
  (void)param;

  for(;;)
  {
    pthread_mutex_lock(&mtx);
    if (flag)
    {
      pthread_mutex_unlock(&mtx);
      break;
    }
    pthread_mutex_unlock(&mtx);
    printf("THREAD\n");
    sleep(1);
  }

  return 0;
}

static void* thread_flagsetter(void* param)
{
  (void)param;
  char pipe_msg[5];
  read(signal_pipe[0], pipe_msg, sizeof(pipe_msg));
  if (strcmp(pipe_msg, "go"))
  {
    fprintf(stderr, "Illegal command ignored.\n");
    return 0;
  }
  pthread_mutex_lock(&mtx);
  flag = true;
  pthread_mutex_unlock(&mtx);
  return 0;
}

static void handler(int sig)
{
  (void)sig;
  printf("SIGINT received.\n");
  write(signal_pipe[1], "go", 3);
}

int main(void)
{
  pthread_t thread_id_sleeper;
  pthread_t thread_id_flagsetter;
  struct sigaction const sigintAction = {
    .sa_handler = handler
  };

  pipe(signal_pipe);
  sigaction(SIGINT, &sigintAction, 0);

  pthread_create(&thread_id_sleeper, 0 /* attr */, thread_sleeper, 0 /* param */);
  pthread_create(&thread_id_flagsetter, 0 /* attr */, thread_flagsetter, 0 /* param */);

  pthread_join(thread_id_sleeper, 0);
  pthread_join(thread_id_flagsetter, 0);
  close(signal_pipe[0]);
  close(signal_pipe[1]);

  return EXIT_SUCCESS;
}
