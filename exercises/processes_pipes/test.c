#include "funcs.h"
#include <unity.h>

void setUp(void)
{
}

void tearDown(void)
{
}

static void fileListTest(void)
{
  char buf[128];
  runProcesses(buf, sizeof(buf));
  TEST_ASSERT_EQUAL_STRING_MESSAGE("12", buf, "wc-Process output is wrong.");
}

static void noBufferTest(void)
{
  char buf[128] = { '\0' };
  runProcesses(buf, 0);
  TEST_ASSERT_EQUAL_STRING_MESSAGE("", buf, "Buffer is not empty.");
}

int main(void)
{
  UNITY_BEGIN();
  RUN_TEST(fileListTest);
  RUN_TEST(noBufferTest);
  return UNITY_END();
}
