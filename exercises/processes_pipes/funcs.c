#include "funcs.h"

#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdio.h>

RC runProcesses(char* buf, size_t bufsize)
{
  int pipe_ls[2];
  int pipe_wc[2];
  pid_t pid_ls;
  pid_t pid_wc;

  pipe(pipe_ls);
  pid_ls = fork();
  switch (pid_ls) {
    case -1:
      return RC_ERROR;
    case 0:
      close(pipe_ls[0]);
      if (pipe_ls[1] != STDOUT_FILENO)
      {
        dup2(pipe_ls[1], STDOUT_FILENO);
        close(pipe_ls[1]);
      }
      execlp("ls", "ls", (char *) NULL);
    default:
      break;
  }

  pipe(pipe_wc);
  pid_wc = fork();
  switch (pid_wc) {
    case -1:
      return RC_ERROR;
    case 0:
      close(pipe_ls[1]);
      if (pipe_ls[0] != STDIN_FILENO)
      {
        dup2(pipe_ls[0], STDIN_FILENO);
        close(pipe_ls[0]);
      }
      close(pipe_wc[0]);
      if (pipe_wc[1] != STDOUT_FILENO)
      {
        dup2(pipe_wc[1], STDOUT_FILENO);
        dup2(pipe_wc[1], STDERR_FILENO);
        close(pipe_wc[1]);
      }
      execlp("wc", "wc", "-l", (char *) NULL);
    default:
      break;
  }

  close(pipe_ls[0]);
  close(pipe_ls[1]);

  waitpid(pid_ls, 0, 0);
  waitpid(pid_wc, 0, 0);

  ssize_t cnt = read(pipe_wc[0], buf, bufsize);
  buf[cnt - 1] = '\0';

  close(pipe_wc[1]);
  close(pipe_wc[0]);

  return RC_OK;
}
