#ifndef FUNCS_H
#define FUNCS_H

#include <stddef.h>
#include <stdint.h>

typedef uint32_t RC;
#define RC_OK               0x00000000
#define RC_ERROR            0x10000000

RC runProcesses(char* buf, size_t bufsize);

#endif /* FUNCS_H */
