#include "funcs.h"

#include <stdlib.h>
#include <stdio.h>

int main(void)
{
  char buf[128];
  RC rc = runProcesses(buf, sizeof(buf));
  fprintf(stdout, "%s\n", buf);
  return rc == RC_OK ? EXIT_SUCCESS : EXIT_FAILURE;
}
