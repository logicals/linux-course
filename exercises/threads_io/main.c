#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
static bool flag = false;
static int signal_pipe[2];

void* thread_sleeper(void* param)
{
  (void)param;

  uint8_t randNum;
  int randFd = open("/dev/urandom", O_RDONLY);
  if (randFd == -1)
  {
    fprintf(stderr, "Cannot open handle for /dev/urandom\n");
    return 0;
  }

  for(;;)
  {
    pthread_mutex_lock(&mtx);
    if (flag)
    {
      pthread_mutex_unlock(&mtx);
      break;
    }
    pthread_mutex_unlock(&mtx);
    if (read(randFd, &randNum, sizeof(randNum)) == -1)
    {
      fprintf(stderr, "Cannot read from /dev/urandom\n");
    }
    printf("Current number: %d\n", randNum);
    sleep(1);
  }

  close(randFd);

  return 0;
}

static void* thread_flagsetter(void* param)
{
  (void)param;
  char read_data[5];
  read(signal_pipe[0], read_data, sizeof(read_data));
  if (strcmp(read_data, "stop"))
  {
    fprintf(stderr, "Illegal command ignored.\n");
    return 0;
  }
  pthread_mutex_lock(&mtx);
  flag = true;
  pthread_mutex_unlock(&mtx);
  return 0;
}

static void handler_int(int sig)
{
  (void)sig;

  /* REMARK: using printf in signal handlers is NOT safe.
   * Here, printf() is only used to show that the signal
   * has been processed.
   */
  printf("SIGINT received.\n");
  write(signal_pipe[1], "stop", strlen("stop") + 1);
}

static void handler_term(int sig)
{
  (void)sig;

  /* REMARK: using printf in signal handlers is NOT safe.
   * Here, printf() is only used to show that the signal
   * has been processed.
   */
  printf("SIGTERM received.\n");
}

int main(void)
{
  pthread_t thread_id_sleeper;
  pthread_t thread_id_flagsetter;
  struct sigaction const sigintAction = {
    .sa_handler = handler_int
  };
  struct sigaction const sigtermAction = {
    .sa_handler = handler_term
  };

  pipe(signal_pipe);
  sigaction(SIGINT, &sigintAction, 0);
  sigaction(SIGTERM, &sigtermAction, 0);

  pthread_create(&thread_id_sleeper, 0 /* attr */, thread_sleeper, 0 /* param */);
  pthread_create(&thread_id_flagsetter, 0 /* attr */, thread_flagsetter, 0 /* param */);

  pthread_join(thread_id_sleeper, 0);
  pthread_join(thread_id_flagsetter, 0);
  close(signal_pipe[0]);
  close(signal_pipe[1]);

  return EXIT_SUCCESS;
}
